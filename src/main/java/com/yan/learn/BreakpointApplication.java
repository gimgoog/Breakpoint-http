package com.yan.learn;

import com.yan.learn.service.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

/**
 * 启动类
 * Created by wenwen on 2017/4/11.
 * version 1.0
 */
@SpringBootApplication(scanBasePackages = "com.yan.learn")
@EnableAutoConfiguration
public class BreakpointApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(BreakpointApplication.class, args);
    }

    @Bean
    CommandLineRunner init(final StorageService storageService) {
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                storageService.deleteAll();
                storageService.init();
            }
        };
    }

}
